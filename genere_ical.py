# -*- coding: utf-8 -*-
'''
Génération d'un fichier iCal avec les réunion de bureau EcoInfo

Le fichier d'entrées doit être de la forme :

31/08/2023 14:00
01/09/2023 9:30
[...]

En première ligne, il peut y avoir le format des lignes si on veut un autre format en 
commençant la première ligne par "#"

#%A %d %B %Hh%M
lundi 04 septembre 9h
lundi 11 septembre 9h30
[...]

Si le format ne comprends pas l'année ("%y" ou "%Y"), c'est l'année en courus qui est utilisée.
Si l'heure de début est en fin de ligne et n'a pas les minutes, "00" est ajouté pour accepter
la ligne.
'''

from datetime import datetime, timedelta
import locale
import configparser
from icalendar import Calendar, Event, vText

FORMAT_DEFAUT = '%d/%m/%Y %H:%M'


def cree_evenement(titre, debut, duree, description, lieu):
    '''
    Creation d'un évènement à intégrer dans un calendrier'''
    event = Event()
    event.add('summary', titre)
    event.add('description', description)
    event.add('dtstart', debut)
    event.add('duration', duree)
    event['location'] = vText(lieu)
    return event


def main():
    """Application principale"""

    locale.setlocale(locale.LC_TIME, '')

    config = configparser.ConfigParser()
    config.read("modele.ini")
    titre = config.get('message-generique', 'titre')
    lieu = config.get('message-generique', 'lieu')
    description = config.get('message-generique', 'description')
    duree = timedelta(minutes=config.getint('message-generique', 'duree'))

    # Initialisation du calendrier
    cal = Calendar()
    cal.add('prodid', '-//My calendar product//mxm.dk//')
    cal.add('version', '2.0')

    # Lecture du fichier texte d'évènements et ajout dans le calendrier
    with open("dates.txt", "rt", encoding='utf8') as handle:
        liste = handle.readlines()
        handle.close()

    if liste[0].startswith('#'):
        format_date = liste.pop(0).lstrip('#').strip()
    else:
        format_date = FORMAT_DEFAUT
    # On permet d'oublier les minutes quand elles sont à 0
    if format_date.endswith('%M'):
        fin = format_date.rstrip('%M')[-1]
    else:
        fin = None
    # Si pas d'année, on prend l'année en cours
    if '%Y' not in format_date and '%y' not in format_date:
        annee = str(datetime.now().year)
        format_date += ' %Y'
    else:
        annee = None

    for ligne in liste:
        ligne = ligne.strip()
        if ligne:
            try:
                ligne_mod = ligne
                if fin is not None and ligne_mod.endswith(fin):
                    ligne_mod += "00"
                if annee is not None:
                    ligne_mod += " " + annee
                debut = datetime.strptime(ligne_mod, format_date)
                evt = cree_evenement(titre, debut, duree, description, lieu)
                cal.add_component(evt)
            except ValueError:
                print("Ligne non reconnue : " + ligne)

    # Écriture du fichier ICal
    with open('bureaux_ecoinfo.ics', 'wb') as handle:
        handle.write(cal.to_ical())
        handle.close()


if __name__ == "__main__":
    main()
